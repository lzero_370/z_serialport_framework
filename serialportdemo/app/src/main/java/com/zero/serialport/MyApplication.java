package com.zero.serialport;

import android.app.Application;
import android.content.Intent;

import com.zero.serialport.service.CardService;

import org.xutils.BuildConfig;
import org.xutils.common.util.LogUtil;
import org.xutils.x;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //开启xutils框架
        x.Ext.init(this);
        x.Ext.setDebug(BuildConfig.DEBUG); // 是否输出debug日志, 开启debug会影响性能.

        //随app启动servies
        Intent intent = new Intent(getApplicationContext(), CardService.class);
        intent.putExtra("command","init");
        startService(intent);

        LogUtil.d("app init...");
    }
}
