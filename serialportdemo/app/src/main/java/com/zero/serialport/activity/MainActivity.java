package com.zero.serialport.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.zero.serialport.R;
import com.zero.serialport.service.CardService;

import org.xutils.common.util.LogUtil;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;
@ContentView(R.layout.activity_main)
public class MainActivity extends Activity {
    @ViewInject(R.id.btn_start)
    private Button btn_start;
    @ViewInject(R.id.btn_end)
    private Button btn_end;
    @ViewInject(R.id.tv_result)
    private TextView tv_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

    }

    @Event(R.id.btn_start)
    private void start(View v){
        Intent intent = new Intent(MainActivity.this, CardService.class);
        intent.putExtra("command","start");
        startService(intent);
        LogUtil.d("点击了start");
    }
    @Event(R.id.btn_end)
    private void end(View v){
        Intent intent = new Intent(MainActivity.this, CardService.class);
        intent.putExtra("command","end");
        startService(intent);
        LogUtil.d("点击了end");
    }

}
